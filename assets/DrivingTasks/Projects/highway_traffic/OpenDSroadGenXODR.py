#!/usr/bin/env/ python

end = """     </planView>
        <elevationProfile>
            <elevation s="0.0000000000000000e+00" a="9.5000000000000000e+00" b="0.0000000000000000e+00" c="0.0000000000000000e+00" d="0.0000000000000000e+00"/>
        </elevationProfile>
        <lateralProfile>
        </lateralProfile>
        <lanes>
            <laneSection s="0.0000000000000000e+00">
                <left>
                    <lane id="1" type="border" level= "false">
                        <link>
							<predecessor id="1"/>
							<successor id="1"/>
                        </link>
                        <width sOffset="0.0000000000000000e+00" a="1.5000000000000000e+00" b="0.0000000000000000e+00" c="0.0000000000000000e+00" d="0.0000000000000000e+00"/>
                        <roadMark sOffset="0.0000000000000000e+00" type="solid" weight="standard" color="standard" width="1.3000000000000000e-01"/>
                    </lane>
                </left>
                <center>
                    <lane id="0" type="driving" level= "false">
                        <link>
                        </link>
                        <roadMark sOffset="0.0000000000000000e+00" type="solid" weight="standard" color="standard" width="1.3000000000000000e-01"/>
                    </lane>
                </center>
                <right>
                    <lane id="-1" type="driving" level= "false">
                        <link>
							<predecessor id="-1"/>
							<successor id="-1"/>
                        </link>
                        <width sOffset="0.0000000000000000e+00" a="3.2" b="0.0000000000000000e+00" c="0.0000000000000000e+00" d="0.0000000000000000e+00"/>
                        <roadMark sOffset="0.0000000000000000e+00" type="broken" weight="standard" color="standard" width="1.3000000000000000e-01"/>
                        <speed sOffset="0.0000000000000000e+00" max="70.0" unit="km/h" />
                    </lane>
                    <lane id="-2" type="driving" level= "false">
                        <link>
							<predecessor id="-2"/>
							<successor id="-2"/>
                        </link>
                        <width sOffset="0.0000000000000000e+00" a="3.2" b="0.0000000000000000e+00" c="0.0000000000000000e+00" d="0.0000000000000000e+00"/>
                        <roadMark sOffset="0.0000000000000000e+00" type="broken" weight="standard" color="standard" width="1.3000000000000000e-01"/>
                        <speed sOffset="0.0000000000000000e+00" max="70.0" unit="km/h" />
                    </lane>
                    <lane id="-3" type="driving" level= "false">
                        <link>
							<predecessor id="-3"/>
							<successor id="-3"/>
                        </link>
                        <width sOffset="0.0000000000000000e+00" a="3.2" b="0.0000000000000000e+00" c="0.0000000000000000e+00" d="0.0000000000000000e+00"/>
                        <roadMark sOffset="0.0000000000000000e+00" type="solid" weight="standard" color="standard" width="1.3000000000000000e-01"/>
                        <speed sOffset="0.0000000000000000e+00" max="70.0" unit="km/h" />
                    </lane>
                    <lane id="-4" type="border" level= "false">
                        <link>
							<predecessor id="-4"/>
							<successor id="-4"/>
                        </link>
                        <width sOffset="0.0000000000000000e+00" a="1.5000000000000000e+00" b="0.0000000000000000e+00" c="0.0000000000000000e+00" d="0.0000000000000000e+00"/>
                        <roadMark sOffset="0.0000000000000000e+00" type="none" weight="standard" color="standard" width="1.3000000000000000e-01"/>
                    </lane>
                </right>
            </laneSection>
        </lanes>
        <objects>
        </objects>
        <signals>
        </signals>
    </road>

</OpenDRIVE>
"""

def roadWriter(maxLen=10000):
    # write init s
    global sInit
    road, len = geometryGen(maxLen)
    header = """<?xml version="1.0" standalone="yes"?>
<OpenDRIVE>
    <header revMajor="1" revMinor="1" name="%s" version="1.00" date="%s" north="1.9000000000000000e+03" south="-1.1500000000000000e+03" east="3.3000000000000000e+03" west="-4.8000000000000000e+02">
    </header>
    <road name="s000" length="%f" id="1" junction="-1">
        <link>

	
        </link>
        <planView>
""" % ("D4C_XODR_generator", datetime.now().strftime("%y-%m-%d-%H-%M"), len)

    header+="""
            <geometry s="0.0" x="0.0" y="0.0" hdg="0.0" length="%s">
				<line/>
			</geometry>
    """% sInit
    # intersections
    road+=end

    roadFile = open("opends.xodr","w")
    roadFile.write("%s" % header)
    roadFile.write("%s" % road)
    roadFile.close()

    print(" --------------- road file generated ---------------- ")

def geometryGen(maxLen):
    global sInit
    x=sInit; y=0; hdg=0; s=sInit
    minCurv = 0.002

    path=" "
    while(s<float(maxLen)):
        c=random.uniform(-minCurv,minCurv)
        l=random.randint(200,500)
        r=math.inf if c==0 else 1/c
        theta = l*c
        hdgLimit=np.pi/2
        if(hdg + theta >= hdgLimit):
            l = (-hdg + hdgLimit)/c
        elif(hdg + theta < -hdgLimit):
            l = -(hdg + hdgLimit)/c
        theta = l*c
        path+="""
  	<geometry s=\"%f\" x=\"%f\" y=\"%f\" hdg=\"%f\" length=\"%f\">
				<arc curvature=\"%f\"/>
			</geometry>
    """ % (s, x, y, hdg, l, c)
        dx=(l if c==0 else r*math.sin(theta))
        dy=(0 if c==0 else r*(1-math.cos(theta)))
        x+=(dx*math.cos(hdg)-dy*math.sin(hdg))
        y+=(dx*math.sin(hdg)+dy*math.cos(hdg))
        hdg+=theta
        s+=l
    path+="""
    <geometry s=\"%f\" x=\"%f\" y=\"%f\" hdg=\"%f\" length="100">
				<line/>
			</geometry>
    """ % (s, x, y, hdg)
    return path, s+100

if __name__ == "__main__":
    import sys
    import random
    import numpy as np
    import math
    from datetime import datetime
    
    minCurv = 0.08
    sInit = 200

    nArg = len(sys.argv)
    if nArg == 1:
        print(" ----------- generate a default long road -----------")
        roadWriter()
    if nArg == 2:
        roadLen = float(sys.argv[1])
        print(" ----------- generate a %sm long road -----------" % roadLen)
        roadWriter(roadLen)

    