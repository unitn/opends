#!/usr/bin/env/ python

end = """     </traffic>

	<road>

	</road>

        <coordinateConversion></coordinateConversion>

    </scenario>
"""

header = """<?xml version="1.0" encoding="UTF-8"?>
<scenario xmlns="http://opends.eu/drivingtask/scenario"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xsi:schemaLocation="http://opends.eu/drivingtask/scenario ../../Schema/scenario.xsd">
        
        <environment>
        	<weather>
                <snowingPercentage>-1</snowingPercentage>
        		<rainingPercentage>-1</rainingPercentage>
        		<fogPercentage>-1</fogPercentage>
        	</weather>
        </environment>

        <driver>
        	<car id="" ref="driverCar">
        	    <resetPoints>
        			<resetPoint ref="reset1" />
        			<resetPoint ref="reset2" />
        			<resetPoint ref="reset3" />
        			<resetPoint ref="reset4" />
        			<resetPoint ref="reset5" />
        		</resetPoints>
        		<tires>
        			<type>winter</type>
        			<size>16</size>
        		</tires>
        		<engine>
        			<engineOn>true</engineOn>
        			<minSpeed>0</minSpeed>
					<maxSpeed>180</maxSpeed>
					<acceleration>3.3</acceleration>
					<minRPM>750</minRPM>
					<maxRPM>7500</maxRPM>
        		</engine>
        		<light>
        			<intensity>0.0</intensity>
        		</light>
        		<transmission>
        			<automatic>true</automatic>
        			<reverse>3.182</reverse>
        			<forward>
	        			<vector jtype="java_lang_Float" size="6">
							<entry>3.615</entry>
							<entry>1.955</entry>
							<entry>1.281</entry>
							<entry>0.973</entry>
							<entry>0.778</entry>
							<entry>0.646</entry>
						</vector>
					</forward>
        		</transmission>
        		<suspension>
					<stiffness>120</stiffness>
					<compression>0.2</compression>
					<damping>0.3</damping>
        		</suspension>
        		<wheel>
        			<frictionSlip>50</frictionSlip>
        		</wheel>
        		<brake>
        			<decelerationFreeWheel>2.0</decelerationFreeWheel>
        			<decelerationBrake>8.7</decelerationBrake>
        		</brake>
        	</car>
        	<openDrive>

        		<startRoadID>1</startRoadID>
        		<startLane>-3</startLane>
        		<startS>10</startS>

        	</openDrive>
        </driver>
        
        <traffic>
"""

def trafficWriter():
    # write init s

    body = header

    nCars = random.randint(10,30)
    id = 1
    startS_hist = []

    for i in range(nCars):
        startLane = random.randint(1,3)
        startS = random.randint(10,60)
        startS*=10
        speed  = random.randint(30,100)
        while(startS in startS_hist):
            startS+=10

        body+="""
        <ODvehicle id="car%i">
				<modelPath>Models/Cars/drivingCars/CitroenC4/Car.j3o</modelPath>
				<mass>800</mass>
				<acceleration>1.2</acceleration>
				<decelerationBrake>8.7</decelerationBrake>
				<decelerationFreeWheel>2.0</decelerationFreeWheel>
				<engineOn>true</engineOn>
				<distanceFromPath>5.0</distanceFromPath>
				<maxSpeed>%i</maxSpeed>
				<neverFasterThanSteeringCar>false</neverFasterThanSteeringCar>

				<startRoadID>1</startRoadID>
				<startLane>-%i</startLane>
				<startS>%i</startS>
			</ODvehicle>
        
        """% (id, speed, startLane, startS)
        startS_hist.append(startS)
        id+=1

    body+=end

    print(startS_hist)

    roadFile = open("scenario.xml","w")
    roadFile.write("%s" % body)
    roadFile.close()

    print(" --------------- traffic file generated ---------------- ")


if __name__ == "__main__":
    import sys
    import random

    minCurv = 0.08
    sInit = 200

    trafficWriter()

'''
    nArg = len(sys.argv)
    if nArg == 1:
        print(" ----------- generate a default long road -----------")
        trafficWriter()
    if nArg == 2:
        roadLen = float(sys.argv[1])
        print(" ----------- generate a %sm long road -----------" % roadLen)
        trafficWriter(roadLen)
'''
    